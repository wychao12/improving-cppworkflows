// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UObject/ObjectMacros.h"
#include "Person.h"
#include "Student.generated.h"

/**
 * 
 */
UENUM()
enum class EAttackType : uint8
{
	Light,
	Heavy
};

UCLASS(DefaultConfig)
class USINGDATA_API UStudent : public UPerson
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "StudentInfo")
	FString Name = "ZhangSan";
	UFUNCTION(BlueprintCallable, Category = "StudentFunc")
	int Study();
	UFUNCTION(BlueprintCallable, Category = "StudentFunc")
	void Play(const float Time);
};
