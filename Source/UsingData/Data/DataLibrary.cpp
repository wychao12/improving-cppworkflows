// Fill out your copyright notice in the Description page of Project Settings.


#include "DataLibrary.h"
#include "Misc/PackageName.h"
#include "../Student.h"

bool UDataLibrary::SaveTable(FString SaveDirectory, FString FileName, UDataTable * DataTable, bool AllowOverwriting)
{
	SaveDirectory += "\\";
	SaveDirectory += FileName;
	if (!DataTable) { return false; }
	if (!AllowOverwriting)
	{
		if (FPlatformFileManager::Get().GetPlatformFile().FileExists(*SaveDirectory))
		{
			return false;
		}
	}
	return FFileHelper::SaveStringToFile(DataTable->GetTableAsCSV(), *SaveDirectory);
}

bool UDataLibrary::ObjectSerialize(UObject* object)
{
	FString PackageName = TEXT("/Game/Blueprints/DataItems/actorAerialize");
	UPackage* package = CreatePackage(*PackageName);
	AActor* actor = NewObject<AActor>(package);
	actor->Rename(TEXT("actorAerialize"));
	FString PackageFileName = FPackageName::LongPackageNameToFilename(PackageName, FPackageName::GetAssetPackageExtension());
	bool bSave = UPackage::SavePackage(package, actor, EObjectFlags::RF_Public | EObjectFlags::RF_Standalone,
		*PackageFileName, GError, nullptr, true, true, SAVE_NoError);
	return bSave == true ? true : false;
}

bool UDataLibrary::ObjectDeserialize()
{
	AActor* actor = LoadObject<AActor>(nullptr, TEXT("/Game/Blueprints/DataItems/actorAerialize.actorAerialize"));
	return actor == nullptr ? false : true;
}


void UDataLibrary::GetClassName()
{
	UStudent* Student = NewObject<UStudent>();
	UClass* StudentClass = Student->GetClass();
	FName ClassName = StudentClass->GetFName();
	UE_LOG(LogTemp, Warning, TEXT("Student's class Name is %s"), *ClassName.ToString());
}

void UDataLibrary::GetClassProperty()
{
	UStudent* Student = NewObject<UStudent>();
	UClass* StudentClass = Student->GetClass();
	for (FProperty* Property = StudentClass->PropertyLink; Property; Property = Property->PropertyLinkNext)
	{
		FString PropertyName = Property->GetName();
		FString PropertyType = Property->GetCPPType();

		// 获取类内的所有字符串属性
		if (PropertyType == "FString")
		{
			// 把属性转为字符串属性， FStrProperty: Describes a dynamic string variable.
			FStrProperty* StringProperty = CastField<FStrProperty>(Property);
			// 获取提供的“容器”中属性值的指针。
			void* Addr = StringProperty->ContainerPtrToValuePtr<void>(Student);

			// 获取属性值
			FString PropertyValue = StringProperty->GetPropertyValue(Addr);
			UE_LOG(LogTemp, Warning, TEXT("Student's property has %s, Type is %s, Value is %s"), *PropertyName, *PropertyType, *PropertyValue);

			// 设置属性值
			StringProperty->SetPropertyValue(Addr, "LiSi");
			PropertyValue = StringProperty->GetPropertyValue(Addr);
			UE_LOG(LogTemp, Warning, TEXT("Student's property has %s, Type is %s, Value is %s"), *PropertyName, *PropertyType, *PropertyValue);
			
			// 获取元数据
			const TMap<FName, FString>* MetaSpecifier = StringProperty->GetMetaDataMap();
			//FString CategoryName = StringProperty->GetMetaData(TEXT("Category"));

			UE_LOG(LogTemp, Warning, TEXT("USeniorStudent's variable %s has metadata: "), *PropertyName);
			if (MetaSpecifier)
			{
				for (auto Item : *MetaSpecifier)
				{
					UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *Item.Key.ToString(), *Item.Value);
				}
			}
		}
	}
}

void UDataLibrary::GetClassFunction()
{
	UStudent* Student = NewObject<UStudent>();
	UClass* StudentClass = Student->GetClass();
	for (TFieldIterator<UFunction> IteratorOfFunction(StudentClass); IteratorOfFunction; ++IteratorOfFunction)
	{
		UFunction* Function = *IteratorOfFunction;
		FString FuncName = Function->GetName();
		EFunctionFlags FuncFlags = Function->FunctionFlags;
		UE_LOG(LogTemp, Warning, TEXT("Student's function has %s with flags: %x"), *FuncName, FuncFlags);
		// 获取该方法对应的参数
		for (TFieldIterator<FProperty> IteratorOfParam(Function); IteratorOfParam; ++IteratorOfParam)
		{
			FProperty* Param = *IteratorOfParam;
			FString ParamName = Param->GetName();
			FString ParamType = Param->GetCPPType();

			UE_LOG(LogTemp, Warning, TEXT("ParamName: %s, ParamType: %s"), *ParamName, *ParamType);

			// 获取参数标记：与类中每个属性关联的标志，覆盖属性的默认行为。
			EPropertyFlags ParamFlags = Param->GetPropertyFlags();
			UE_LOG(LogTemp, Warning, TEXT("ParamFlags is %x"), ParamFlags);

			UE_LOG(LogTemp, Warning, TEXT("----------------"));
		}
	}
}

void UDataLibrary::GetParentName()
{
	UStudent* Student = NewObject<UStudent>();
	UClass* ParentClass = Student->GetClass()->GetSuperClass();
	FString ParentClassName = ParentClass->GetName();
	UE_LOG(LogTemp, Warning, TEXT("MiddleStudent Parent Class Name: %s"), *ParentClassName);
}

bool UDataLibrary::IsSubclass()
{
	UClass* Class2 = UPerson::StaticClass(); // 另外一种获取UClass的方法
	UClass* Class1 = UStudent::StaticClass();
	UClass* Class3 = AActor::StaticClass();

	if (Class1->IsChildOf(Class2))
	{
		UE_LOG(LogTemp, Warning, TEXT("Class1 is Class2's Child."));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Class1 is not Class2's Child."));
	}

	if (Class3->IsChildOf(Class2))
	{
		UE_LOG(LogTemp, Warning, TEXT("Class3 is Class2's Child."));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Class3 is not Class2's Child."));
	}
	return false;
}

void UDataLibrary::GetAllChild()
{
	TArray<UClass*> ClassResults;

	GetDerivedClasses(UPerson::StaticClass(), ClassResults, false);
	for (auto ChildClass : ClassResults)
	{
		UE_LOG(LogTemp, Warning, TEXT("UPerson's derivedchild has %s"), *ChildClass->GetName());
	}
}

void UDataLibrary::FindClassAndEnum()
{
	UClass* FindedClass = FindObject<UClass>(ANY_PACKAGE, TEXT("Student"), true);

	if (FindedClass)
	{
		UE_LOG(LogTemp, Warning, TEXT("Class With Name: UMiddleStudent has found."));
	}
	UE_LOG(LogTemp, Warning, TEXT("------------Finds a enum based on the given string------------"));
	UEnum* FindedEnum = FindObject<UEnum>(ANY_PACKAGE, TEXT("EAttackType"), true);

	if (FindedEnum)
	{
		UE_LOG(LogTemp, Warning, TEXT("Class With Name: EAttackType has found."));

		// 遍历枚举项名称
		for (int32 Index = 0; Index < FindedEnum->NumEnums(); Index++)
		{
			UE_LOG(LogTemp, Warning, TEXT("EAttackType Name: %s."), *FindedEnum->GetNameStringByIndex(Index));
		}
	}
}

void UDataLibrary::FindBlueprintClass()
{
	
}

void UDataLibrary::IsNativeOrBlueprint()
{
	UBlueprint* FindedBP = FindObject<UBlueprint>(ANY_PACKAGE, TEXT("BP_Student"));
	if (FindedBP->IsNative())
	{
		UE_LOG(LogTemp, Warning, TEXT("BP_Test is native class."));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("BP_Test is blueprint class."));
	}

	if (UStudent::StaticClass()->IsNative())
	{
		UE_LOG(LogTemp, Warning, TEXT("UStudent::StaticClass() is native class."));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UStudent::StaticClass() is blueprint class."));
	}
}

void UDataLibrary::GetClassMeta()
{
	UStudent* Student = NewObject<UStudent>();
	UClass* StudentClass = Student->GetClass();
	bool bHasFlags = StudentClass->HasAnyClassFlags(EClassFlags::CLASS_DefaultConfig);
	if (bHasFlags)
	{
		UE_LOG(LogTemp, Warning, TEXT("Student's class specifier has MinimalAPI"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Student's class specifier not has MinimalAPI"));
	}
}

void UDataLibrary::IsContainFunction()
{
	UStudent* Student = NewObject<UStudent>();
	UClass* StudentClass = Student->GetClass();
	if (StudentClass)
	{
		UFunction* PlayFunc = StudentClass->FindFunctionByName(TEXT("Study"), EIncludeSuperFlag::ExcludeSuper);
		if (PlayFunc)
		{
			UE_LOG(LogTemp, Warning, TEXT("GoodStudent class has Function: %s"), *PlayFunc->GetName());
		}
	}
}

void UDataLibrary::ProceeEvent()
{
	UStudent* Student = NewObject<UStudent>();
	UClass* StudentClass = Student->GetClass();
	if (StudentClass)
	{
		UFunction* PlayFunc = StudentClass->FindFunctionByName(TEXT("Play"), EIncludeSuperFlag::ExcludeSuper);
		if (PlayFunc)
		{
			UE_LOG(LogTemp, Warning, TEXT("GoodStudent class has Function: %s"), *PlayFunc->GetName());
			// 1. 给方法所有参数分配空间,并初始化
			uint8* AllFuncParam = static_cast<uint8*>(FMemory_Alloca(PlayFunc->ParmsSize));
			FMemory::Memzero(AllFuncParam, PlayFunc->ParmsSize);
			// 2. 给所有参数赋值
			for (TFieldIterator<FProperty> IteratorOfParam(PlayFunc); IteratorOfParam; ++IteratorOfParam)
			{
				FProperty* FuncParam = *IteratorOfParam;
				FString FuncParamName = FuncParam->GetName();

				if (FuncParamName == TEXT("Time"))
				{
					*FuncParam->ContainerPtrToValuePtr<float>(AllFuncParam) = 60.0;
				}
			}

			// 3. 调用方法
			Student->ProcessEvent(PlayFunc, AllFuncParam);
		}

		// 通过Invoke调用类方法
		UFunction* GoHomeFunc = StudentClass->FindFunctionByName(TEXT("Study"), EIncludeSuperFlag::ExcludeSuper);
		if (GoHomeFunc)
		{
		//	// 1. 给方法所有参数分配空间,并初始化
			uint8* AllFuncParam = static_cast<uint8*>(FMemory_Alloca(GoHomeFunc->ParmsSize));
			FMemory::Memzero(AllFuncParam, GoHomeFunc->ParmsSize);

		//	// 2. 创建FFrame
			FFrame Frame(nullptr, GoHomeFunc, &AllFuncParam);

		//	// 3. 调用Invoke
			GoHomeFunc->Invoke(Student, Frame, &AllFuncParam + GoHomeFunc->ReturnValueOffset);

		//	// 4. 获取返回值
			int* ResValue = (int*)(&AllFuncParam + GoHomeFunc->ReturnValueOffset);
			UE_LOG(LogTemp, Warning, TEXT("GoHome Function return value: %d"), *ResValue);

		}
	}
}




