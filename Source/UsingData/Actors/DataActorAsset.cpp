// Fill out your copyright notice in the Description page of Project Settings.


#include "DataActorAsset.h"
#include "../Student.h"

#if WITH_EDITOR
void ADataActorAsset::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{

	Super::PostEditChangeProperty(PropertyChangedEvent);
	if (!bRunCode || ItemType == EItemType::None) { return; }
	if (ItemDataAsset)
	{
		for (int i = 0; i < ItemDataAsset->ItemInfoArray.Num(); i++)
		{
			if (ItemDataAsset->ItemInfoArray[i].ItemType == ItemType)
			{
				GenericItemInfo = ItemDataAsset->ItemInfoArray[i];

				Mesh->SetStaticMesh(GenericItemInfo.Mesh);
				mFloatCurve = GenericItemInfo.CurveFloat;
				mRotationDuration = GenericItemInfo.RotationDuration;
				break;
			}
		}
	}
}
#endif

void ADataActorAsset::GetObjectByClass()
{
	UStudent* Student1 = NewObject<UStudent>(this, FName(TEXT("WangWu")));
	UStudent* Student2 = NewObject<UStudent>(this, FName(TEXT("ZhaoLiu")));
	TArray<UObject*> ObjectsResults;
	GetObjectsOfClass(UStudent::StaticClass(), ObjectsResults, false);
	for (auto Object : ObjectsResults)
	{
		UE_LOG(LogTemp, Warning, TEXT("UStudent's object has %s"), *Object->GetName());
	}
}
