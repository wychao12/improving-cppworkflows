// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class SLATEDEV_API SWidgetDemoA : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWidgetDemoA){}
	SLATE_ATTRIBUTE(FString,InText)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

private:
	//点击按错后的反馈
	void OnLoginButtonClicked(FString usn,FString pwd);
};
