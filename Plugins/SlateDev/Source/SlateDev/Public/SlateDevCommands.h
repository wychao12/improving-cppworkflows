// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "SlateDevStyle.h"

class FSlateDevCommands : public TCommands<FSlateDevCommands>
{
public:

	FSlateDevCommands()
		: TCommands<FSlateDevCommands>(TEXT("SlateDev"), NSLOCTEXT("Contexts", "SlateDev", "SlateDev Plugin"), NAME_None, FSlateDevStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > OpenPluginWindow;
};