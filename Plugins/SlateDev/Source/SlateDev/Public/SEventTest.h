// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
DECLARE_DELEGATE_TwoParams(FLoginDelegate, FString, FString);
class SLATEDEV_API SEventTest : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SEventTest){}
	SLATE_EVENT(FLoginDelegate,OnStartLogin)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);
private:
	FReply OnLoginButtonClicked();
	FLoginDelegate OnLoginDelegate;
	TSharedPtr<SButton> LoginButtonPtr;
	TSharedPtr<SEditableTextBox> UserNamePtr;
	TSharedPtr<SEditableTextBox> PasswordPtr;
};
