// Fill out your copyright notice in the Description page of Project Settings.


#include "SWidgetDemoA.h"
#include "SlateOptMacros.h"
#include "SEventTest.h"
BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWidgetDemoA::Construct(const FArguments& InArgs)
{
	FString InString = InArgs._InText.Get();
	UE_LOG(LogTemp, Warning, TEXT("Attribute is %s"), *InString);
	ChildSlot
	[
		SNew(SEventTest).OnStartLogin(this,&SWidgetDemoA::OnLoginButtonClicked)
		
	];
	
}
void SWidgetDemoA::OnLoginButtonClicked(FString usn, FString pwd)
{
	//执行具体的代码
	UE_LOG(LogTemp, Warning, TEXT("Start Login %s - %s"),*usn,*pwd);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
