// Fill out your copyright notice in the Description page of Project Settings.


#include "SEventTest.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
#define LOCTEXT_NAMESPACE "MyNamespace"
void SEventTest::Construct(const FArguments& InArgs)
{
	OnLoginDelegate = InArgs._OnStartLogin;
	
	ChildSlot.Padding(50, 50, 50, 50)
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SAssignNew(UserNamePtr, SEditableTextBox)
			.HintText(LOCTEXT("Username_Hint", "Please Input Username"))
		]
	+ SVerticalBox::Slot().AutoHeight()
		[
			SAssignNew(PasswordPtr, SEditableTextBox)
			.IsPassword(true)
		.HintText(LOCTEXT("Password_Hint", "Please Input Password"))
		]
	+ SVerticalBox::Slot().AutoHeight()
		[
			SAssignNew(LoginButtonPtr, SButton)
			.OnClicked(this, &SEventTest::OnLoginButtonClicked)
		.Text(LOCTEXT("Login", "Login"))
		]
	];
	
}
FReply SEventTest::OnLoginButtonClicked()
{
	FString usn = UserNamePtr->GetText().ToString();
	FString pwd = PasswordPtr->GetText().ToString();
	OnLoginDelegate.ExecuteIfBound(usn, pwd);
	return FReply::Handled();
}
#undef LOCTEXT_NAMESPACE
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
