﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "SlateDev.h"
#include "SlateDevStyle.h"
#include "SlateDevCommands.h"
#include "LevelEditor.h"
#include "Widgets/Docking/SDockTab.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Text/STextBlock.h"
#include "ToolMenus.h"
#include "SWidgetDemoA.h"

static const FName SlateDevTabName("SlateDev");

#define LOCTEXT_NAMESPACE "FSlateDevModule"

void FSlateDevModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	//初始化UI样式
	FSlateDevStyle::Initialize();
	FSlateDevStyle::ReloadTextures();

	//注册命令
	FSlateDevCommands::Register();

	//实例化一个命令，并进行行为绑定
	PluginCommands = MakeShareable(new FUICommandList);
	PluginCommands->MapAction(
		FSlateDevCommands::Get().OpenPluginWindow,
		FExecuteAction::CreateRaw(this, &FSlateDevModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FSlateDevModule::RegisterMenus));
	
	//注册一个Tab容器
	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(SlateDevTabName, FOnSpawnTab::CreateRaw(this, &FSlateDevModule::OnSpawnPluginTab))
		.SetDisplayName(LOCTEXT("FSlateDevTabTitle", "SlateDev"))
		.SetMenuType(ETabSpawnerMenuType::Hidden);
}

void FSlateDevModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FSlateDevStyle::Shutdown();

	FSlateDevCommands::Unregister();

	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(SlateDevTabName);
}

TSharedRef<SDockTab> FSlateDevModule::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{
	//FText WidgetText = FText::Format(
	//	LOCTEXT("WindowWidgetText", "Add code to {0} in {1} to override this window's contents"),
	//	FText::FromString(TEXT("FSlateDevModule::OnSpawnPluginTab")),
	//	FText::FromString(TEXT("SlateDev.cpp"))
	//	);

	//return SNew(SDockTab)
	//	.TabRole(ETabRole::NomadTab)
	//	[
	//		// Put your tab content here!
	//		SNew(SBox)
	//		.HAlign(HAlign_Center)
	//		.VAlign(VAlign_Center)
	//		[
	//			SNew(STextBlock)
	//			.Text(WidgetText)
	//		]
	//	];
	
	return SNew(SDockTab).TabRole(ETabRole::NomadTab)
		[
			SNew(SWidgetDemoA).InText(FString("Hello Slate"))
		];
}

void FSlateDevModule::PluginButtonClicked()
{
	FGlobalTabmanager::Get()->TryInvokeTab(SlateDevTabName);
}

void FSlateDevModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FSlateDevCommands::Get().OpenPluginWindow, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FSlateDevCommands::Get().OpenPluginWindow));
				Entry.SetCommandList(PluginCommands);
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FSlateDevModule, SlateDev)