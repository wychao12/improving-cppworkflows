// Copyright Epic Games, Inc. All Rights Reserved.

#include "SlateDevCommands.h"

#define LOCTEXT_NAMESPACE "FSlateDevModule"

void FSlateDevCommands::RegisterCommands()
{
	UI_COMMAND(OpenPluginWindow, "SlateDev", "Bring up SlateDev window", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
